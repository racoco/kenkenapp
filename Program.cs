﻿
using System;
using System.Diagnostics;
using System.Linq;

namespace KenKenApp
{
    public class Program
    {   
        static int Main(string[] args)
        {
            Console.WriteLine("KenKenApp starting up...");
            KenKenMain app;
            int isok = -1;  // -1 (Exception), 0 (Success), 1 (No Solution Found)

            try
            {
                Stopwatch s = new Stopwatch();
                s.Start();

                if (args == null || args.Length == 0)   app = new KenKenMain();
                else app = new KenKenMain( args[0] );
                app.Solve();

                s.Stop();
                TimeSpan ts = s.Elapsed;
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                        ts.Hours, ts.Minutes, ts.Seconds,
                        ts.Milliseconds / 10);                   
                Console.WriteLine("KenKenApp finished executing.");
                Console.WriteLine($"Elapsed Time. (H:M:S.ms) : {elapsedTime}");

                if (app.Solution == null)
                {   
                    isok = 1;
                    Console.WriteLine("No solution found!");
                }
                else
                {
                    isok = 0;
                    Console.WriteLine("Solution found:");
                    app.PrintSolution();
                }
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine($"ArgumentException: {ex.Message}");
            }  
            catch (ApplicationException ex)
            {
                Console.WriteLine($"ApplicationException: {ex.Message}");
            } 
            catch (Exception ex)
            {
                Console.WriteLine($"Unexpected Exception: {ex.Message}");
            }

            return isok;
        }
    }
}
