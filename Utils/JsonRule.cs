namespace KenKenApp.Utils
{
    /// <summary>
    /// Part of the Json-to-Class conversion
    /// </summary>
    public class JsonRule
    {
        public string group;
        public int value;
        public string op;
    }

}