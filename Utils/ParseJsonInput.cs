
using Newtonsoft.Json;

namespace KenKenApp.Utils
{
    /// <summary>
    /// Convert Json input string into Class data
    /// </summary>
    public class ParseJsonInput
    {
       public JsonData Data;

        public ParseJsonInput(){ }

        public ParseJsonInput(string jsonString)
        {
            Data = JsonConvert.DeserializeObject<JsonData>(jsonString);
        }

        public static JsonData Parse(string jsonString)
        {
            return JsonConvert.DeserializeObject<JsonData>(jsonString);
        }
    }

}
