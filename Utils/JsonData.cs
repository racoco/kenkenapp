
using System.Collections.Generic;
using System.Text;

namespace KenKenApp.Utils
{
    /// <summary>
    /// Class into which the Json input string is converted 
    /// </summary>

    public class JsonData
    {
        public IEnumerable<JsonRule> rules = new List<JsonRule>();

        public IEnumerable<string[]> grid = new List<string[]>();
        
        public int dimension;

        public override string ToString()
        {
            StringBuilder buf = new StringBuilder("Dimension: " + dimension.ToString() + System.Environment.NewLine);

            buf.Append("Rules (group, value, operation)=:" + System.Environment.NewLine);
            foreach (JsonRule r in rules)
            {
                buf.Append( "(" + r.group + ", " + r.value+ ", " + r.op + ")"
                             + System.Environment.NewLine );
            }

            buf.Append("Grid Layout:" + System.Environment.NewLine);
            foreach ( string[] l in grid )
            {
                foreach( string s in l )
                {
                    buf.Append( s + " ");
                }
                buf.Append( System.Environment.NewLine );
            }

            return buf.ToString();
        }
    }

}