using System;
using Xunit;
using KenKenApp;
using System.IO;
using KenKenApp.Utils;
using System.Collections.Generic;

namespace xUnit.Tests
{
    public class UnitTest1
    {
        //[Fact]
        [Theory]
        [InlineData("../../../../DefaultBoard.json", 6, 14)]
        [InlineData("../../../TestPuzzle7x7.json", 7, 22)]
        public void JsonParseTest(string jsonPath, int expectedDim, int expectedNumRules)
        {
            string json = File.ReadAllText(jsonPath);
            Assert.NotNull(json);
            Console.WriteLine("***** Json Input:" + System.Environment.NewLine + json);

            JsonData data = ParseJsonInput.Parse(json);
            Assert.NotNull(data);

            Console.WriteLine("***** JsonData Object:" + System.Environment.NewLine + data.ToString());

            Assert.Equal(expectedDim, data.dimension);
            Assert.Equal(expectedDim, ((List<string[]>)(data.grid)).Count );
            Assert.Equal(expectedNumRules, ((List<JsonRule>)(data.rules)).Count ); 
        }

        [Theory]
        [InlineData(3, true)]
        [InlineData(0, false)]
        [InlineData(10, false)]
        public void CheckGridDimensionTest(int gridSize, bool expectedResult)
        {
            Board b = new Board();
            Assert.Equal( expectedResult, b.ValidateSize(gridSize));
        }

        [Theory]
        [InlineData("../../../TestPuzzle7x7.json")]
        [InlineData("../../../TestPuzzle8x8.json")]
        public void FullRunTest(string jsonInputFile)
        {
            Console.WriteLine($"Running Test using Json Input File {jsonInputFile}...");
            KenKenMain app = new KenKenMain( jsonInputFile );
            app.Solve();
            Console.WriteLine("Computed Solution:");
            app.PrintSolution();
        }
    }
}
