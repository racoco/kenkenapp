using System;
using System.Collections.Generic;
using System.Linq;

namespace KenKenApp
{
    public class Rule
    {
        public enum Op { plus, minus, times, divide };
        public Rule(string l, int v, Op o) { letters = l; value = v; op = o; }
        public Rule() { }
        public string letters { get; set;}
        public int value { get; set;}
        public Op op { get; set;}
        
        public bool Eval(Board board, int[][] rows)
        {
            List<int> vals = new List<int>();
            for (int row = 0; row < rows.Length; row++)
                for (int col = 0; col < board.GridSize; col++)
                    if (board.Grid[row,col] == letters)
                        vals.Add(rows[row][col]);
                    
            //Debug.Assert(vals.Count>0);         // Put back for debugging
            
            if ( vals.Count == 0 )
                throw new ApplicationException("No values added to list!");
            
            switch (op)
            {
                case Op.plus:
                    return vals.Sum() == value;
                case Op.times:
                    return vals.Aggregate(1, (x, y) => x * y) == value;
                case Op.minus:
                    if(vals.Count > 2)
                        throw new ApplicationException("Can't Minus more than two cells (" + letters + ")");
                    return (vals[0] - vals[1] == value) || (vals[1] - vals[0] == value);
                case Op.divide:
                    if (vals.Count > 2)
                        throw new ApplicationException("Can't Divide more than two cells (" + letters + ")");
                    return (vals[0] * value == vals[1]) || (vals[1] * value == vals[0]);    //avoid int division
                default:
                    throw new ApplicationException($"Invalid arithmetic operation: {op}");
            }
        }
    }
}
