using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using KenKenApp.Utils;

namespace KenKenApp
{
    /// <summary>
    /// Where the algorithms involved in the solution reside.
    /// </summary>
    public class KenKenMain
    {
        //public int GridSize { get; set; }  // From input file

        private Board Board;
        public List<int[]> Permutations;
        public List<Rule>[] RulesByRow;
        public int[][] Solution;

        // Use default built-in board
        public KenKenMain()
        {
            Board = new Board(); 
            Init();
        }

        /// <summary>
        /// Import Board from Json file.
        /// </summary>
        /// <param name="fileName">Path to Json file Board description</param>
        /// <throws>IOException (or other System exceptions) if the specified file cannot be imported</throws>
        /// <throws>ArgumentException if specified file path is null</throws>
        public KenKenMain(string fileName)
        {
            if (fileName == null)  
                throw new ArgumentException("Json file path argument was NULL");

            ReadInputFile(fileName);
            Init();
        }
      
        /// <summary>
        /// Open KenKen board description file. 
        /// First entry is puzzle dimension.
        /// Following lines are the arithmetic constraints.
        /// Check it is all consistent.
        /// Load data in Board
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public void ReadInputFile(string fileName)
        {
            // open file and read in the specified json puzzle-board representation.
            // transform it into a data class and set up the Board
            // Check it obeys dimension constraints.
            // Set Board.GridSize Board.Rules, Board.Grid vrbls
            string json = File.ReadAllText(fileName);
            JsonData data = ParseJsonInput.Parse(json);
            Board = new Board(data);
        }

        private void Init()
        {
            RulesByRow = new List<Rule>[Board.GridSize];
            Permutations = new List<int[]>();
        }

        /// <summary>
        /// Solve the Puzzle. 
        /// The Board data should have already been imported and validated
        /// </summary>
        public void Solve()
        {
            AnalyzeRules();
            CreateRowPermutations();
            Solution = null;
            TryRowSolution( new int[0][]);

            Board.Solution = Solution;

            //Debug.WriteLine(Solution==null ? "No solution" : Solution.Aggregate("", (s, r) => s + r.Aggregate("", (ss,c)=> ss+","+c.ToString())+"\n"));
        }

        /// <summary>
        /// Sort rules by the last row they apply to so that they can be 
        /// checked as soon as that row is available and thus fail as early
        /// as possible.
        /// </summary>
        private void AnalyzeRules()
        {
            var rulesRemaining = Board.Rules.Select(r => r.letters).ToList();
            for (int row = Board.GridSize - 1; row >= 0; row--)
            {
                RulesByRow[row] = new List<Rule>();
                for (int col = 0; col < Board.GridSize; col++) 
                    if (rulesRemaining.Contains(Board.Grid[row, col]))
                    {
                        Rule rule = Board.Rules.First(r => r.letters == Board.Grid[row, col]);
                        RulesByRow[row].Add(rule);
                        rulesRemaining.Remove(rule.letters);
                    }
            }
        }

        private void CreateRowPermutations()
        {
            int[] left = new int[0];
            int[] right = new int[Board.GridSize];
            for(int i=0; i < Board.GridSize; i++)   right[i] = i + 1;

            Permute(left, right);
        }

        /// <summary>
        /// Create each permutation by appending each element of 'right' 
        /// to 'left' and recursing
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        private void Permute(IEnumerable<int> left, IEnumerable<int> right)
        {
            if (left.Count() == Board.GridSize)
            {
                Permutations.Add(left.ToArray());
                return;
            }
            for (int i = 0; i < right.Count(); i++)
                Permute( left.Concat( new int[] { right.ElementAt(i)} ), 
                         right.Where((val, index) => index != i).ToArray());
        }

        private bool TryRowSolution(int[][] rows)
        {
            if (rows.Count() == Board.GridSize)   //bottom-out
            {
                Solution = rows.ToArray();
                return true;
            }
            int newrow = rows.Count();
            foreach (int[] permute in Permutations)
            {
                if (IsValid(rows, permute)) //check new row is consistent
                {
                    int[][] newgrid = rows.Concat(new int[][] { permute }).ToArray();
                    if (RulesByRow[rows.Count()].All(r => r.Eval(Board, newgrid.ToArray())))  //test each rule
                        if (TryRowSolution(newgrid))
                            return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Is this permutation of numbers in the next row consistent with 
        /// the existing rows so far
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="permute"></param>
        /// <returns></returns>
        private bool IsValid(int[][] rows, int[] nextRow)
        {
            for (int col = 0; col < Board.GridSize; col++)
                for (int row = 0; row < rows.Count(); row++)
                {
                    if (nextRow[col] == rows[row][col])
                        return false;
                }
            return true;
        }

        public void PrintSolution()
        {
            if ( Solution == null || Solution.Length == 0)
                Console.WriteLine("No Solution data available.");
            Console.WriteLine(Solution.Aggregate("", (s, r) => 
                        s + r.Aggregate("", (ss,c)=> ss + " " + c.ToString()) + "\n"));
        }
    }
}