What is it
----------

    A KenKen analyser. Input a Board, it solves it (min 2x2, max 10x10).

    This is a generalization of a solution by Terry Spitz:

      https://www.codeproject.com/Articles/31574/KenKen-Solver-in-WPF

    It is "generalized" in that it allows a KenKen Board to be specified as a
    JSON input file that specifies the initial layout and associated arithmetic
    operations. In principle, there is no limit to the size and layout of a
    KenKen board. For practical reasons, we limit the input to 10x10.

    I was learning how to use vscode on Linux and revist some rusty C# skills.
    Being a KenKen fan, I thought hijacking Spitz's solution would be a fun
    way to do that. I made it a simple console app (not interested in the GUI 
    development aspects) which can read in a JSON representation of a KenKen
    Board. This was a great way to get me introduced to vscode and see an 
    interesting KenKen implementation. See the file TestPuzzle7x7.json for an
    example of a JSON representation of a KenKen board.

What I had to do to get this working using vscode
-------------------------------------------------

By "this", I mean a vscode netcoreapp3.1 Console application on Fedora Linux 35.

I found vscode 3.1 was a bitch to get working properly in a Fedora Linuxenvironment, 
especially the  unit code framework.

Used these tutorials:
    https://www.pluralsight.com/guides/testing-.net-core-apps-with-visual-studio-code

Big problem: getting mstest framework to work.

I had to:
    * Make sure "MSTest.TestAdapter" and "MSTest.Testframework" were of the same
      version.

    * Add PackageReference XML Elements to *both* KenKenApp.csprog and 
      KenKenApp.Tests.csprog, otherwise building failed at root level.

    * Make sure that only the netcoreapp3.1 versions of things were being 
      referenced. I even completely deleted ~/.nuget/Packages so that it would
      contain this project's NuGets upon a 'dotnet restore' and dotnet clean
      and rebuild sequence.

Nothing mattered! The UnitTest Framework will not recognize and run the tests:
For example, from within the KenKenApp.Tests directory:

    dotnet test
    Test run for /home/racoco/Development/projects/VSCodeApps/KenKenApp/KenKenApp.Tests/bin/Debug/netcoreapp3.1/KenKenApp.Tests.dll(.NETCoreApp,Version=v3.1)
    Microsoft (R) Test Execution Command Line Tool Version 16.3.0-dev+55e7e45431c9c05656c999b902686e7402664573
    Copyright (c) Microsoft Corporation.  All rights reserved.

    Starting test execution, please wait...

    A total of 1 test files matched the specified pattern.

    No test is available in /home/racoco/Development/projects/VSCodeApps/KenKenApp/KenKenApp.Tests/bin/Debug/netcoreapp3.1/KenKenApp.Tests.dll. 
    Make sure that test discoverer & executors are registered and platform & 
    framework version settings are appropriate and try again.

Very strange, since it at first seemed to have ID'd the 1 existing test file.

I tried adding to settings.json in an attempt to get mstest to work; eg,
      dotnet-test-explorer.testProjectPath": "**/*Tests.csproj"
Did not matter. Nothing mattered. It simply will not work for me.

I assume there may be a version mismatch among the assemblies.

Finally, I added an xUnit test project. And it *just worked*! [WTF!]
At least, running "dotnet test" in the xUnit.Tests project directory.
The Test Explorer complains about 'duplicate ID'. I think this is related to 
the case-sensitivity of the Linux OS wrt directory names? F**king Windows 
technology...

Rich Coco 
January 2022

Dev reminders...

  * Run a particular test
        dotnet test --filter "FullyQualifiedName=xUnit.Tests.UnitTest1.FullRunTest"
