using System;
using System.Diagnostics;
using System.Linq;
using KenKenApp.Utils;

namespace KenKenApp
{
    public class Board
    {
         public static readonly int MaxGridSize = 10;
        public static readonly int MinGridSize = 3;
        public static readonly int DefaultGridDimension = 6;
        public static readonly string[,] DefaultGrid =
        {
            {"A","A","B","B","B","C"},
            {"A","D","D","D","E","C"},
            {"F","F","D","G","H","H"},
            {"I","I","J","G","K","K"},
            {"L","L","J","J","J","M"},
            {"L","L","N","N","N","M"}
        };

        public static readonly Rule[] DefaultRules =
        {
            new Rule( "A", 8,  Rule.Op.times),
            new Rule( "B", 13, Rule.Op.plus ),
            new Rule( "C", 3,  Rule.Op.divide ),
            new Rule( "D", 17, Rule.Op.plus ),
            new Rule( "E", 6,  Rule.Op.plus ),
            new Rule( "F", 1,  Rule.Op.minus ),
            new Rule( "G", 3,  Rule.Op.minus ),
            new Rule( "H", 7,  Rule.Op.plus ),
            new Rule( "I", 9,  Rule.Op.plus ),
            new Rule( "J", 17, Rule.Op.plus ),
            new Rule( "K", 3,  Rule.Op.minus ),
            new Rule( "L", 17, Rule.Op.plus ),
            new Rule( "M", 2,  Rule.Op.divide ),
            new Rule( "N", 6,  Rule.Op.plus )
        };

        public string[,] Grid;
        public Rule[] Rules;
        public int GridSize;

        public int[][] Solution;

        public Board() 
        {
            Grid = Board.DefaultGrid;
            Rules = Board.DefaultRules;
            GridSize = Board.DefaultGridDimension;
            Validate();
        }

        /// <summary>
        /// Construct Grid, Rules, and size from the transformed Json data
        /// </summary>
        /// <param name="data"></param>
        public Board(JsonData data)
        {
            if ( data == null )    
                throw new ArgumentException("Null JsonData object passed to Board");
            
            if ( ! ValidateSize(data.dimension) )
                throw new ArgumentException("Invalid dimension specified in JsonData");

            JsonDataToBoardData(data);
            Validate();
        }

        /// <summary>
        /// Populate local Board data structures with the imported Json data
        /// </summary>
        /// <param name="data"></param>
        /// <throws>ArgumentException if Rule op is invalid.
        public void JsonDataToBoardData(JsonData data)
        {
            int row = 0;
            int col = 0;

            GridSize = data.dimension;
                Debug.WriteLine($"**** GridSize = {GridSize}");

            Grid= new string[GridSize,GridSize];
            foreach ( string[] l in data.grid )
            {
                    Debug.WriteLine($"*** l = {l.ToString()}  sizeof(l) = {l.Length}");
                    for(int xx=0; xx < GridSize; xx++)  Debug.WriteLine($"l[{xx}] = {l[xx]}");
                col = 0;
                foreach( string s in l )
                {
                        Debug.WriteLine($"**** Row {row}: col {col}: --> {s}");
                    Grid[row,col] = s;   // Recall, each string s is a single letter.
                    col++;
                }
                row++;
            }

            int numRules = data.rules.Count();
            Rules = new Rule[numRules];
            int k = 0;
            foreach (JsonRule r in data.rules)
            {
                Rule.Op o;
                if (r.op.Equals("*")) o = Rule.Op.times;
                else if (r.op.Equals("+")) o = Rule.Op.plus;
                else if (r.op.Equals("-")) o = Rule.Op.minus;
                else if (r.op.Equals("/")) o = Rule.Op.divide;
                else  throw new ArgumentException($"Invalid operator ({r.op.ToString()}) specified  in Rule {k}");
                Rules[k++] = new Rule(r.group, r.value, o);
            }
        }

        public bool Validate()
        {
            bool isok = true;

            if ( ! ValidateRules() )
            {
              Console.WriteLine("Rule validation failed.");
               isok = false;
            }

            if ( ! ValidateSize(GridSize) )
            {
                Console.WriteLine($"Specified grid size {GridSize} not in permitted range [${MinGridSize}, ${MaxGridSize}]");
                isok = false;
            }

            if ( ! isok )    throw new ArgumentException("The Board failed validation");
            return true;
        }

         /// <summary>
        /// Check that every grid letter has a rule
        /// </summary>
        /// <returns></returns>
        public bool ValidateRules()
        {
            for (int row = 0; row < GridSize; row++)
                for (int col = 0; col < GridSize; col++)
                    if (!Rules.Any(r => r.letters == Grid[row, col]))
                        return false;
            return true;
        }

        public bool ValidateSize(int size)
        {
            return ( (    (size > MaxGridSize)
                       || (size < MinGridSize) )
                    ? false : true );
        }
    }
}
